import { Controller, Get } from '@nestjs/common';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
    constructor(
        private readonly userService: UserService
    ){}

    @Get()
    async root(): Promise<any>{
        const res = await this.userService.createDefaultUser();
        return res;
    }
}
