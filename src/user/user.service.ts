import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UserService {
    constructor (
        @Inject('UserRepositoryToken') private readonly userRepository: Repository<User>
    ) {}

    async createDefaultUser (): Promise<User> {
        const user = new User();
        user.firstName = 'FN';
        user.lastName = 'LN';
        user.email = `${user.firstName.toLowerCase()}.${user.lastName.toLowerCase()}@mail.com`;
        user.password = '11111';
        return await this.userRepository.save(user)
    }
}
